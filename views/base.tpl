<!doctype html>
<html>
	<head>
		{{template "head" .}}
	</head>
	<body>
		<script data-main="/static/" src="/static/jam/require.js"></script>
		<div class="title">
			<h1><a href="http://essocony.bandcamp.com">Essocony Bandcamp</a></h1>
		</div>
		{{template "body" .}}
	</body>
</html>
