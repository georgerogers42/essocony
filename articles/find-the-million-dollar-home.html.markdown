{"Title": "Million Dollar Home", "Slug": "million-bucks", "Date": "2015-07-10 07:58 CDT"}

<h2 id="million-dollar-homes-answer">Which is the million dollar house</h2>
<table id="million-dollar-homes">
  <tr>
		<th><a href="https://www.redfin.com/TX/Sugar-Land/43-Sterling-St-77479/home/32835833">This 6000 square foot mansion in Sugar Land TX</a></th>
    <td>
      <img src="/static/not-a-million.jpg" alt="Wrong! This mansion is a mere $600,000 in Sugar Land, TX.">
    </td>
  </tr>
  <tr>
		<th><a href="http://www.zillow.com/homedetails/630-San-Benito-Ave-Menlo-Park-CA-94025/15579370_zpid/">This 2400 square foot "mansion" in Menlo Park CA</a></th>
    <td>
      <img src="/static/million.jpg" alt="Correct, This piece of garbage from the 1940's will cost you a dear 1.4 million dollars in Menlo Park, CA.">
    </td>
  </tr>
</table>
<script src="/static/imgclick.js"></script>
