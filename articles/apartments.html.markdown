{"Title": "But Car?", "Slug": "but-car", "Date": "2015-07-17 13:10 CDT"}

<h2>Would you rather</h2>
<table>
	<tr>
		<th>Houston</th>
		<td>
			<table>
				<tr> 
					<td>Live</td>
					<td><img src="/static/kitchen-hou.jpg"></td>
				</tr>
				<tr>
					<td>Drive</td>
					<td><img src="/static/3.jpg"></td>
				</tr>
				<tr>
					<td>On</td>
					<td><img src="/static/katystack.jpeg"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th>New York City</th>
		<td>
			<table>
				<tr>
					<td>Live</td>
					<td><img src="/static/kitchen-nyc.jpg"></td>
				</tr>
				<tr>
					<td>Ride</td>
					<td><img src="/static/subway.jpg"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<h2>Both can be had at the same price</h2>
<table><tr><th>Place</th><th>Apartment</th><th>Car</th><th>Gas</th><th>Transit</th><th>Total</th></tr><tr><td>Houston TX</td><td>$1,500.00</td><td>$992.00</td><td>$418.50</td><td>$0.00</td><td>$2,910.50</td></tr><tr><td>New York NY</td><td>$2,800.00</td><td>$0.00</td><td>$0.00</td><td>$119.00</td><td>$2,919.00</td></tr></table>
