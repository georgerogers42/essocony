package esso

import (
	"bufio"
	"encoding/json"
	"github.com/russross/blackfriday"
	"html/template"
	"os"
	"sync"
	"time"
)

type ArticleInfo struct {
	Author, Title, Slug, Date string
}

func (a ArticleInfo) Time() time.Time {
	t, err := time.Parse("2006-01-02 15:04 MST", a.Date)
	if err != nil {
		panic(err)
	}
	return t
}

type Article struct {
	ArticleInfo
	Text template.HTML
}

type ArticleList []*Article

func (a ArticleList) Len() int {
	return len(a)
}
func (a ArticleList) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a ArticleList) Less(i, j int) bool {
	return a[i].Time().After(a[j].Time())
}

var Articles map[string]*Article
var ArticlesList ArticleList
var Lock sync.RWMutex

func LoadArticle(fn string) (*Article, error) {
	a := &Article{}
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	m := ""
	for s.Scan() {
		t := s.Text()
		if t == "" {
			json.Unmarshal([]byte(m), &a.ArticleInfo)
			break
		}
		m += t + "\n"
	}
	if e := s.Err(); e != nil {
		return nil, e
	}
	b := make([]byte, 0, 1024)
	for s.Scan() {
		b = append(b, []byte(s.Text()+"\n")...)
	}
	if e := s.Err(); e != nil {
		return nil, e
	}
	a.Text = template.HTML(blackfriday.MarkdownCommon(b))
	return a, nil
}
